from db import dbClass
from security import securityClass
from pprint import pprint

class userClass:
    # Get user information by ID
    # Returns an associative array
    def getByID(self, _userID):
        db = dbClass()
        ret = db.getUserByID(_userID)
        del db
        return ret
    
    # Get user information by Username
    # Returns an associative array
    def getByUsername(self, _username):
        db = dbClass()
        ret = db.getUserByUsername(_username)
        del db
        return ret
    
    # Create a new user
    def create(self, _username, _password, _type):
        db = dbClass()
        sec = securityClass()
        
        # Already a user? Bounce...
        if db.checkIfUserExists(_username) is True:
            return None
        
        # Generate secrets / password hash
        _salt = sec.generateSalt()
        _secret = sec.generateSecret()
        _hash = sec.generateHash(_password, _salt)
        
        # Create the user in the database and return the user ID
        ret = db.addUser(_username, _type, _hash, _salt, _secret)
        del db
        return ret

    # Check user-provided credentials against what's in the database
    def authenticate(self, _username, _password):
        sec = securityClass()
        
        # Get the user information
        usr = self.getByUsername(_username)
        
        # Not a valid user? Not authenticated.
        if usr is None or 'salt' not in usr:
            return None
        
        # Generate a new hash using the user's salt to see if we have a match
        if sec.generateHash(_password, usr['salt']) == usr['password']:
            usr['password'] = None
            usr['salt'] = None
            return usr
        
        return None
