from db import dbClass
from security import securityClass

import datetime

class ticketClass:    
    # Add a new ticket
    def create(self, _userID, _userSecret, _tripFrom, _tripTo, _dateStart, _dateEnd):
        db = dbClass()
        sec = securityClass()
        
        # Generate the public/private ticket pair
        pubTicket = sec.generatePublicTicket()
        privTicket = sec.generatePrivateTicket(pubTicket, _userSecret)
        
        # Add the ticket to the database
        ret = db.addTicket(_userID, privTicket, _tripTo, _tripFrom, _dateStart, _dateEnd)

        # Check that the operation was successful
        if ret is None:
            return None
        
        print '== New Ticket =='
        print 'userID: ' + str(_userID)
        print 'userSecret: ' + _userSecret
        print 'pubTicket: ' + pubTicket
        print 'privTicket: ' + privTicket
        print '================'
        
        del db
        
        return pubTicket
    
    
    # Algorithm: privTicket = pbkdf2(sha512(pubTicket, userSecret), 100000)
    # Returns:
    #  0: Success
    #  1: Unknown user ID
    #  2: Unknown ticket + user ID combination
    #  3: Ticket has already been used
    #  4: Ticket is not yet valid
    #  5: Ticket has expired
    def validate(self, _userID, _userSecret, _pubTicket):
        db = dbClass()
        sec = securityClass()

        # Calculate the resulting ticket
        _privTicket = sec.generatePrivateTicket(_pubTicket, _userSecret)
        
        print '== Ticket Validation =='
        print 'userID: ' + str(_userID)
        print 'pubTicket: ' + _pubTicket
        print 'privTicket: ' + _privTicket
        print '======================='
        
        # See if we have a match
        ticket = db.getTicket(_userID, _privTicket)

        if ticket is None:
            return {'error': 2, 'ticket': None}
        
        # Check if it's been used already or not
        if ticket['used'] is not 0:
            return {'error': 3, 'ticket': None}
        
        # Now check the start/end dates
        date_now = datetime.datetime.utcnow()
        date_start = datetime.datetime.strptime(ticket['dateStart'], "%Y-%m-%d %H:%M:%S.%f")
        date_end = datetime.datetime.strptime(ticket['dateEnd'], "%Y-%m-%d %H:%M:%S.%f")
        
        if date_start > date_now:
            return {'error': 4, 'ticket': None}
        if date_end < date_now:
            return {'error': 5, 'ticket': None}
        
        # Mark the ticket as used
        db.setTicketUsed(_userID, _privTicket)
        
        del db
        
        return {'error': 0, 'ticket': ticket}
    
    # Get all user tickets (without verifying them)
    def getTicketsByUserID(self, _userID):
        # Get a database connection
        db = dbClass()
        return db.getTicketsByUserID(_userID)