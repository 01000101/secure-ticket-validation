Proof-of-Concept service to generate and authenticate event tickets (anything from bus tickets to concert tickets). 

This was used for a client and some parts of the repository have been removed and squashed for confidentiality. 


# Security

## Password storage

The user's password is hashed using SHA-512 using 100,000 PBKDF2 rounds.
This slows down the process of making the final hash and, with the
inclusion of a random salt, near impossible to brute-force and
is not suseptible to rainbow table lookups.


## Tickets

Tickets are presented to the front-end (via QR code) as one part
SHA-244 hash and one part User ID (this example users the users'
database ID but could use their email address instead)
 
The problem being solved with that format is that we can use those
primitives to establish authenticity of the ticket and user. We can
ensure that ticket X issued to user Y cannot be used by user Z.
If a user were clever enough to find out we are using SHA-244 hash
and what the ID of a ticket was (or, most likely, guessed one), they
could not just hash the ID and send it with their user ID because
it would not match our records since it is salted. 

We accomplish this using HMAC hashing. There are actually two tickets
for every booking. There is a 'public' ticket and a 'private' one.
What happens is that the 'public' ticket is simply a random SHA-244
hash that is given to the user (this is their public booking reference)
but it's mostly useless on its own.  This 'public' ticket is
hashed with a key known as the user's 'secret'.  This 'secret' is
generated per user (during registration). During a booking, the
'public' ticket is generated, then it's hashed with the user's 'secret'
which produces a 'private' ticket. This 'private' ticket is what's
actually stored in the database (in the 'tickets' table) as well as
the user's secret (in the 'users' table). The 'public' ticket is given
to the user for use in their QR code or whatever.

In order to verify a taken, we need the user's ID and their 'public'
ticket.  Then we use the following one-way HMAC algorithm:

    privTicket = pbkdf2(sha512(pubTicket,userSecret),100000)
    
If the privTicket matches a 'private' ticket we have on file for that
user then we can check that it's valid and return a yes or no answer.

*What this does NOT protect against*:

If a malicious user gets the QR code itself and staff does not check
name/gender/etc. If the QR code (ticket+ID) is valid and unused,
there is not much we can do to protect against that as they've
essentially committed ID fraud and/or have defeated the security
of your app itself (either stealing the legitimate users' phone
or stealing the data from the phone/email). Not much you can do
at that point other than ID confirmation by staff (see notes above).