import random
import os
import binascii
import hashlib

class securityClass:
    # Generate a user secret for ticket HMAC validation
    def generateSecret(self):
        return os.urandom(random.randint(20, 42)).encode('base_64')
    
    # Generate random salt to thwart rainbow table lookups
    def generateSalt(self):
        return os.urandom(random.randint(12, 18)).encode('base_64')
    
    # HMAC SHA-512 password hash with salt and 100,000 rounds
    def generateHash(self, _password, _salt):
        dk = hashlib.pbkdf2_hmac('sha512', _password, _salt, 100000)
        return binascii.hexlify(dk)
    
    def generatePublicTicket(self):
        # Generate a "ticket" (really just cryptograhic base material)
        # This doesn't need to be cryptographically strong as
        # even in the case of a collision, we're using a user secret
        pubTicket = os.urandom(random.randint(16, 42)).encode('base_64')
        
        # Generate the real ticket (SHA-224)
        return hashlib.sha224(pubTicket).hexdigest()
    
    def generatePrivateTicket(self, pubTicket, userSecret):
        # HMAC SHA-512 hash with user secret and 100,000 rounds
        dk = hashlib.pbkdf2_hmac('sha512', pubTicket, userSecret, 100000)
        return binascii.hexlify(dk)
    
    

    