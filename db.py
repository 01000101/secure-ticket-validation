import sqlite3
import datetime

class dbClass:
    
    def __init__(self):
        # Connect to the SQLite3 database
        self.conn = sqlite3.connect('prototype.db')
        self.cursor = self.conn.cursor()
    
    def checkIfUserExists(self, _username):
        # Check if the user already exists
        self.cursor.execute("SELECT id FROM users WHERE username=?", [_username])
        if self.cursor.fetchone() is None:
            return False
        return True
    
    def addTicket(self, _userID, _ticket, _tripTo, _tripFrom, _dateStart, _dateEnd):
        # Update the database with the new ticket
        # id INTEGER PRIMARY KEY, userid INTEGER, ticket TEXT, used INTEGER, trip_to TEXT, trip_from TEXT, date_start TEXT, date_end TEXT)")
        self.cursor.execute("INSERT INTO tickets (userid,ticket,used,used_ts,trip_to,trip_from,date_start,date_end) VALUES (?,?,?,?,?,?,?,?)",
            (_userID, _ticket, 0, '', _tripTo, _tripFrom, _dateStart, _dateEnd))
        self.conn.commit()
        return self.cursor.lastrowid
    
    def getTicket(self, _userID, _privTicket):
        self.cursor.execute("SELECT date_start,date_end,trip_to,trip_from,used,used_ts FROM tickets WHERE userid=? AND ticket=?",
            (_userID, _privTicket))
        
        row = self.cursor.fetchone()
        if row is not None:
            return {
                'ticket': _privTicket,
                'userid': _userID,
                'dateStart': row[0],
                'dateEnd': row[1],
                'tripTo': row[2],
                'tripFrom': row[3],
                'used': row[4],
                'used_ts': row[5]
            }
        return None
    
    def getTicketsByUserID(self, _userID):
        self.cursor.execute("SELECT id,ticket,used,trip_to,trip_from,date_start,date_end FROM tickets WHERE userid=?", [_userID])
        
        entries = [dict(id=row[0],
            ticket=row[1],
            used=row[2],
            trip_to=row[3],
            trip_from=row[4],
            date_start=datetime.datetime.strptime(row[5], "%Y-%m-%d %H:%M:%S.%f"),
            date_end=datetime.datetime.strptime(row[6], "%Y-%m-%d %H:%M:%S.%f"),
        ) for row in self.cursor.fetchall()]
        
        return entries
    
    
    def setTicketUsed(self, _userID, _privTicket):
        self.cursor.execute("UPDATE tickets SET used=1,used_ts=? WHERE userid=? AND ticket=?",
            (datetime.datetime.utcnow(), _userID, _privTicket))
        self.conn.commit()
        return
    
    
    def addUser(self, _username, _type, _hash, _salt, _secret): 
        if self.checkIfUserExists(_username) is True:
            return None
        
        # Add the new user
        self.cursor.execute("INSERT INTO users (username,password,salt,secret,type,date_created) VALUES (?,?,?,?,?,?)",
            (_username,_hash,_salt,_secret,_type,datetime.datetime.utcnow()))
        self.conn.commit()
        
        # Return the new user's ID (None on error)
        return self.cursor.lastrowid
    
    def getUserByID(self, _userID):
        self.cursor.execute("SELECT id,username,type,salt,secret,password,date_created FROM users WHERE id=?", [_userID])
        row = self.cursor.fetchone()
        if row is not None:
            return {
                'id': row[0],
                'username': row[1],
                'type': row[2],
                'salt': row[3],
                'secret': row[4],
                'password': row[5],
                'date_created': row[6]
            }
        return {}
    
    def getUserByUsername(self, _username):
        self.cursor.execute("SELECT id,username,type,salt,secret,password,date_created FROM users WHERE username=?", [_username])
        row = self.cursor.fetchone()
        if row is not None:
            return {
                'id': row[0],
                'username': row[1],
                'type': row[2],
                'salt': row[3],
                'secret': row[4],
                'password': row[5],
                'date_created': row[6]
            }
        return {}
    
    def initDemo(self):
        print 'Creating database...';
        # Print basic module info
        print 'SQLite3 database version: ' + sqlite3.sqlite_version
        print 'SQLite3 module version: ' + sqlite3.version
        
        # Create a persistent file database
        dbConn = sqlite3.connect('prototype.db')
        dbCursor = dbConn.cursor()
        
        # Create the users table
        #  id: user ID
        #  username: user's registered email address
        #  password: password hash (SHA-512 w/ PBKDF2)
        #  salt: salt for password hash
        #  secret: salt for generated tickets
        #  type: 1 = customer, 2 = staff
        #  date_created: the user creation timestamp
        #  (confirmed): BOOLEAN indicating if the user has confirmed their email address (not implemented)
        dbCursor.execute("CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY, username TEXT, password TEXT, salt TEXT, secret TEXT, type INTEGER, date_created TEXT)")
        # Create the tickets table
        #  id: ticket ID
        #  userid: (relational) user ID that owns the ticket
        #  ticket: ticket hash (issued to user)
        #  used: BOOLEAN indicating if the ticket has been used
        #  date_created: the ticket creation timestamp
        #  date_start: ticket is valid after this timestamp
        #  date_end: ticket is valid until this timestamp
        dbCursor.execute("CREATE TABLE IF NOT EXISTS tickets (id INTEGER PRIMARY KEY, userid INTEGER, ticket TEXT, used INTEGER, used_ts TEXT, trip_to TEXT, trip_from TEXT, date_start TEXT, date_end TEXT)")
        
        # Write the changes
        dbConn.commit()
        print 'Database created successfully'
    
    def __del__(self):
        self.conn.close()