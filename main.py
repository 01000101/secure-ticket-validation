# Custom classes 
from db import dbClass
from security import securityClass
from user import userClass
from ticket import ticketClass

import datetime
import re

from flask import Flask, session
from flask.ext.session import Session
from flask import request, url_for
from flask import redirect, render_template
app = Flask(__name__)

# Configure session settings (they will be stored in /flask-session/*)
SESSION_TYPE = 'filesystem'
app.config.from_object(__name__)
Session(app)

# Create the demo / test database tables 
db = dbClass()
db.initDemo()
del db


# The user must login here before going to any other page besides 'regsiter'
@app.route('/login', methods=['POST', 'GET'])
def api_login():
    error = None
    if request.method == 'POST':
        # I trim start/end whitespace but it's not necessary
        # depending on your policies. Just remember that whatever
        # you do here needs to be replicated everywhere you test
        # usernames / passwords. 
        inEmail = request.form['inEmail']
        inEmail = inEmail.strip()
        inPassword = request.form['inPassword']
        inPassword = inPassword.strip()
        
        # Validate email (this is RFC-compliant but is
        # probably not optimal for real-world application)
        if not re.match(r"[^@]+@[^@]+\.[^@]+", inEmail):
            return render_template('login.html', error='Pleae enter a valid email address')
        
        # Validate password (there should be more here depending on your policies)
        # I'll simply refer you to OWASP's recommendations:
        # https://www.owasp.org/index.php/Authentication_Cheat_Sheet#Implement_Proper_Password_Strength_Controls
        if( len(inPassword) < 10 or len(inPassword) > 128 ):
            return render_template('login.html', error='Password must be between 10 and 128 characters')
        
        # Test the credentials
        user = userClass()
        usrObj = user.authenticate(inEmail, inPassword)
        if usrObj != None and 'id' in userObj:
            # Set up the session and redirect
            session['userid'] = usrObj['id']
            session['username'] = usrObj['username']
            session['usertype'] = usrObj['type']
            return redirect(url_for('api_dashboard'))
        else:
            session.pop('userid', None);
            error = 'Incorrect login information provided'
            
    # the code below is executed if the request method
    # was GET or the credentials were invalid
    if session.get('userid', None) != None:
        return redirect(url_for('api_dashboard'))
    
    return render_template('login.html', error=error)

# The user can register an account here
# This prototype allows users to register as either a customer or as staff
# without additional verification. I'm sure your current production app
# somehow verifies a staff member is really staff but it's not relevant here.
@app.route('/register', methods=['POST', 'GET'])
def api_register():
    # Already logged in? Redirect!
    if session.get('userid', None) != None:
        return redirect(url_for('api_dashboard'))
    
    if request.method == 'POST':
        # Grab the POST'd variables
        inEmail = request.form['inEmail']
        inEmail = inEmail.strip()
        inPassword = request.form['inPassword']
        inPassword = inPassword.strip()
        inType = request.form['inType']
        
        # Validate user type (and force to INT)
        if inType == '1': inType = 1
        elif inType == '2': inType = 2
        else: return render_template('register.html', error='Pleae select a valid option (customer or staff)')
        
        # Validate email
        if not re.match(r"[^@]+@[^@]+\.[^@]+", inEmail):
            return render_template('register.html', error='Pleae enter a valid email address')
        
        # Validate password
        if( len(inPassword) < 10 or len(inPassword) > 128 ):
            return render_template('register.html', error='Password must be between 10 and 128 characters')
    
        # Add the user to the database and check for error
        user = userClass()
        adduserRet = user.create(inEmail, inPassword, inType)
        
        if adduserRet is None:
            return render_template('register.html', error='A user with the provided email address already exists in our database')
    
        # Log the user in and redirect
        # In production this should actually not log them in and
        # instead send them an email to confirm their account
        session['userid'] = adduserRet
        session['username'] = inEmail
        session['usertype'] = inType
        
        return redirect(url_for('api_dashboard'))
    
    return render_template('register.html')

# The user can logout here (ends the session)
@app.route('/logout')
def api_logout():
    # remove the username from the session if it's there
    session.pop('userid', None)
    return redirect(url_for('api_login'))

# Dummy "index" that is really just the login page
@app.route('/')
def api_homepage():
    return redirect(url_for('api_login'))

# This is the users' main dashboard where they can use or validate tickets
# This dashboard is sensitive to the user type (staff or customer). It will
# display different options and you can see this in the actual template itself.
@app.route('/dashboard', methods=['POST', 'GET'])
def api_dashboard():
    # Not logged in? Go away.
    if session.get('userid', None) == None:
        return redirect(url_for('api_logout'))
    # Unknown / missing user type? Go away.
    if session.get('usertype', None) == None:
        return redirect(url_for('api_logout'))
    
    _success = None
    _error = None
    
    ticket = ticketClass()
    
    if request.method == 'POST':
        # Customer user
        if session.get('usertype', None) == 1:
            user = userClass()
            
            # Get the user's secret
            usr = user.getByID(session.get('userid', None))
            
            # Grab the POST'd variables (user)
            inTripFrom = request.form['inTripFrom']
            inTripTo = request.form['inTripTo']
            
            # current date and date + 30 days
            # Obviously this is arbitrary and would be replaced with your own logic
            dateStart = datetime.datetime.utcnow()
            dateEnd = dateStart + datetime.timedelta(days = 30)
            
            # Actually create the ticket
            ret = ticket.create(usr['id'], usr['secret'], inTripFrom, inTripTo, dateStart, dateEnd)
            
            if ret is None:
                _error = "An unexpected error occurred, please try again"
            else:
                # This is seriously the most important peice of information the
                # user needs to have (either in the QR code or in email/etc)
                # Losing this may mean irrecoverable loss of their ticket
                _success = "Your ticket is " + ret + ". Please do not lose this code."
        # Staff user
        elif session.get('usertype', None) == 2:
            user = userClass()
            
            # Grab the POST'd variables (staff)
            inTicket = request.form['inTicket']
            inID = request.form['inID']
            
            # Get the user's secret
            usr = user.getByID(inID)
            
            if 'id' not in usr:
                _error = "Unknown user ID"
            else:
                ret = ticket.validate(usr['id'], usr['secret'], inTicket)
                
                if ret['error'] == 0:
                    _success = "The ticket is valid"
                elif ret['error'] == 1:
                    _error = "Unknown user ID"
                elif ret['error'] == 2:
                    _error = "Invalid ticket + user ID combination"
                elif ret['error'] == 3:
                    _error = "The ticket has already been used"
                elif ret['error'] == 4:
                    _error = "The ticket is not yet valid"
                elif ret['error'] == 5:
                    _error = "The ticket has expired"
                else:
                    _error = "An unknown error occurred"
    
    # Get the customer's ticket history
    entries = []
    if session.get('usertype', None) == 1:
        entries = ticket.getTicketsByUserID(session.get('userid', None))
    
    return render_template('dashboard.html',
        userid=session.get('username', 'Unknown'),
        usertype=session.get('usertype', 0),
        entries=entries,
        success=_success,
        error=_error)


if __name__ == '__main__':
    app.run(debug=True)